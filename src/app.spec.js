import React from 'react';
import { shallow } from 'enzyme';

import App from './app';
import { Home } from './components';

describe('App', () => {
  it('Should render home component', () => {
    const wrapper = shallow(<App />);
    const homeComponent = wrapper.find(Home);

    expect(homeComponent.length).toBe(1);
  });
});
