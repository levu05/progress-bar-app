import axios from 'axios';
import { GET_CONFIG_API_URL } from './home.constant';

export const getConfig = (getConfigSuccess, getConfigError) => {
  axios.get(GET_CONFIG_API_URL)
    .then(response => getConfigSuccess(response.data))
    .catch(error => getConfigError(error));
};

export const getProgressPercentage = (value, limit) => Math.floor((value / limit) * 100);
