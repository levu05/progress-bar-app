import React from 'react';
import classNames from 'classnames/bind';

import { getProgressPercentage } from '../../home.services';

export const ProgressBar = React.memo(({ value, limit, id, selected }) => {
  const percentage = getProgressPercentage(value, limit);
  const exceeded = percentage > 100;
  const className = classNames('progress-bar', {
    'progress-bar--exceeded': exceeded,
    'selected': selected
  });
  const progressWidth = `${exceeded ? 100 : percentage}%`;

  return (
    <div id={id} className={className}>
      <div className='progress-value' style={{ width: progressWidth }}></div>
      <div className='percentage-value'>{percentage}%</div>
    </div>
  );
});
