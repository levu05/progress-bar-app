import React from 'react';
import classNames from 'classnames/bind';

export const ControlPanel = React.memo(({
  buttons = [],
  bars = [],
  onChangeBar,
  onChangeProgress,
  exceeded
}) => {
  return (
    <div className='control-panel'>
      <BarSwitch bars={bars} onChange={onChangeBar}/>
      {
        buttons.map((value, key) =>
          <Button key={key} value={value} onClick={onChangeProgress} exceeded={exceeded}/>
        )
      }
    </div>
  );
});

export const BarSwitch = React.memo(({ bars, onChange }) =>
  <select onChange={onChange} className='bar-switch'>
    {bars && bars.map((value, index) => (
      <option key={index} value={index}>#progress{index + 1}</option>
    ))}
  </select>
);

export const Button = React.memo(({ value, onClick, exceeded }) => {
  const isPlus = value > 0;
  const className = classNames('button', {'button--warning': exceeded && isPlus});

  return (
    <button className={className} onClick={onClick.bind(this, value)} id={value}>
      {isPlus ? `+${value}` : value}
    </button>
  );
});
