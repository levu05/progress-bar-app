import React, { Component } from 'react';
import { isEmpty } from 'lodash';

import { GET_CONFIG_ERROR_MESSAGE, APP_TITLE } from './home.constant';
import { getConfig, getProgressPercentage } from './home.services';
import { ProgressBar, ControlPanel } from './components/';
import './home.style.scss'

export class Home extends Component {
  constructor(props) {
    super(props);

    this.state = {
      bars: [],
      buttons: [],
      limit: 0,
      selectedBarId: 0,
      isSelectedBarExceeded: false
    };
  }

  componentDidMount(){
    getConfig(this.getConfigSuccess, this.getConfigError)
  }

  getConfigSuccess = data => {
    const { selectedBarId } = this.state;
    const { buttons, bars, limit } = data;
    const selectedBarpercentage = getProgressPercentage(bars[selectedBarId], limit);
    const isSelectedBarExceeded = selectedBarpercentage > 100;

    this.setState({ bars, buttons, limit, isSelectedBarExceeded });
  }

  getConfigError = error => {
    console.log(error);
    alert(GET_CONFIG_ERROR_MESSAGE);
  }

  onChangeBar = event => {
    const selectedBarId = Number(event.target.value);
    const { bars, limit } = this.state;
    const selectedBarpercentage = getProgressPercentage(bars[selectedBarId], limit);
    const isSelectedBarExceeded = selectedBarpercentage > 100;

    this.setState({ selectedBarId, isSelectedBarExceeded });
  };

  onChangeProgress = value => {
    const { bars, bars: newBars, selectedBarId, limit } = this.state;
    const prevValue = bars[selectedBarId];
    const newValue = prevValue + value;
    const selectedBarpercentage = getProgressPercentage(newValue, limit);
    const isSelectedBarExceeded = selectedBarpercentage > 100;
    
    newBars[selectedBarId] = newValue >= 0 ? newValue : 0;

    this.setState({ bars: newBars, isSelectedBarExceeded });
  }

  render() {
    const { bars, buttons, limit, isSelectedBarExceeded, selectedBarId } = this.state;

    return (
      <div className='home'>
        { !isEmpty(bars) && 

          <div className='progress-bars-container'>
            <h1>{APP_TITLE}</h1>
            {
              bars.map((value, index) =>
                <ProgressBar
                  key={index}
                  id={index}
                  selected={selectedBarId === index}
                  value={value}
                  limit={limit}
                />
              )
            }
            <ControlPanel
              buttons={buttons}
              bars={bars}
              onChangeBar={this.onChangeBar}
              onChangeProgress={this.onChangeProgress}
              exceeded={isSelectedBarExceeded}
            />
          </div>
        }
      </div>
    )
  }
}
