export const GET_CONFIG_API_URL = 'http://pb-api.herokuapp.com/bars';
export const APP_TITLE = 'Progress Bars Demo';
export const GET_CONFIG_ERROR_MESSAGE = 'Requested configuration unsuccessfully';
