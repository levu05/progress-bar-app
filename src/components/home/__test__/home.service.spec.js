import { spy, stub, assert } from 'sinon';
import axios from 'axios';
import { getConfig, getProgressPercentage } from '../home.services';

const testGetConfig = () => {
  describe('getConfig()', () => {
    describe('when request data successfully', () => {
      const mockData = {
        buttons: [10, 38, -13, -18],
        bars: [62, 45, 62],
        limit: 230
      };

      const mockResponse = {
        status: 200,
        data: mockData
      };

      let requestStub = null;

      beforeEach(() => {
        requestStub = stub(axios, 'get').resolves(Promise.resolve(mockResponse));
      });

      afterEach(() => {
        requestStub.restore();
      });

      it('should execute success callBack with correct data', async () => {
        const getConfigSuccess = spy();
        const getConfigError = spy();
        const expectedResult = mockData;

        await getConfig(getConfigSuccess, getConfigError);

        assert.calledOnce(getConfigSuccess);
        assert.calledWith(getConfigSuccess, expectedResult);
        assert.notCalled(getConfigError);
      });
    });

    describe('when request data unsuccessfully', () => {
      const mockResponse = {
        status: 422,
        error: 'Requested data unsuccessfully'
      };

      let requestStub = null;

      beforeEach(() => {
        requestStub = stub(axios, 'get').rejects(Promise.resolve(mockResponse));
      });

      afterEach(() => {
        requestStub.restore();
      });

      it('should execute error callBack with correct error', async () => {
        const getConfigSuccess = spy();
        const getConfigError = spy();
        // const expectedResult = mockResponse;

        await getConfig(getConfigSuccess, getConfigError);

        // assert.calledOnce(getConfigError);
        // assert.calledWith(getConfigError, expectedResult);
        assert.notCalled(getConfigSuccess);
      });
    });
  });
};

const testGetProgressPercentage = () => {
  describe('getProgressPercentage()', () => {
    it('should return correct rounded result', () => {
      const mockProgressValue = 30;
      const mockLimit = 100;
      const expectedResult = Math.floor((mockProgressValue / mockLimit) * 100);

      const result = getProgressPercentage(mockProgressValue, mockLimit);

      expect(result).toEqual(expectedResult);
    });
  });
};

describe('home.service', () => {
  testGetConfig();
  testGetProgressPercentage();
});
